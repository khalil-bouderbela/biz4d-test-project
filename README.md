# Biz4d Test Project
The application have a 3-type user system;

·User - able to see their own profile, create and edit their own projects.

·Manager - able to see their own profile, view existing projects of all users.

·Super Admin - able to see their own profile, view, edit and delete all users, all managers as well as all projects.


## Authors

- [@khalilbouderbela](https://github.com/khalilbouderbela)

## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Buttons Color | ![#36C95D](https://via.placeholder.com/10/0a192f?text=+) #36C95D |
| Design Color | ![#7A0E6D](https://via.placeholder.com/10/f8f8f8?text=+) #7A0E6D |


## Related

Here are some related projects

[Awesome README](https://github.com/matiassingers/awesome-readme)


## Documentation
[Authentication](https://laravel.com/docs/7.x/authentication)
[Middleware](https://laravel.com/docs/8.x/middleware)
[Google Graph](https://developers.google.com/chart/interactive/docs/gallery/piechart#donut)


## Usage/Examples

```php
php artisan db:seed --clas=AccountsSeeder

This command will create 3 type of users accounts to use them for login :
********** Admin Account **********
Email: admin@gmail.com
Password: 12341234
***********************************

********** Manager Account **********
Email: manager@gmail.com
Password: 12341234
***********************************

********** User Account **********
Email: user@gmail.com
Password: 12341234
***********************************
```
